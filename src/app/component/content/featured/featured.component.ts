import { Component, OnInit } from '@angular/core';
import { GetMovieDetailsService } from '../../../services/data-access/get-movie-details.service';
import { forkJoin } from 'rxjs/';
import { SearchobjectModel } from '../../models/searchobject.model';

@Component({
  selector: 'app-featured',
  templateUrl: './featured.component.html',
  styleUrls: ['./featured.component.css']
})
export class FeaturedComponent implements OnInit {

  constructor(private getMovieDetailsService: GetMovieDetailsService) { }
  limit = 200;
  featuredMovieList = [];
  ngOnInit() {
    this.getFeaturedMovies();
  }

   getFeaturedMovies(): void {
     const movie1 = this.getMovieDetailsService.fetchMovieDetails(this.selectAFeaturedMovies());
     const movie2 = this.getMovieDetailsService.fetchMovieDetails(this.selectAFeaturedMovies());
     forkJoin([movie1, movie2]).subscribe(results => {
       this.featuredMovieList.push(results[0]);
       this.featuredMovieList.push(results[1]);
     });
   }

   showMore() {
    if (this.limit === 200) {
      this.limit = undefined;
    } else {
      this.limit = 200;
    }
   }

   showFeaturedMovies(response): void {
    this.featuredMovieList = response;
   }

   selectAFeaturedMovies(): SearchobjectModel {
      const movieList = [
        {
          movieName : 'Deadpool',
          plot : 'Full'
        }, {
          movieName : 'Titanic',
          plot : 'Full'
        }, {
          movieName : 'Moana',
          plot : 'Full'
        }, {
          movieName : 'Despicable Me',
          plot : 'Full'
        }, {
          movieName : 'Red Sparrow',
          plot : 'Full'
        }];

      return movieList[Math.floor(Math.random() * movieList.length)];
   }
}
