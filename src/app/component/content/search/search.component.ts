import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { GetMovieDetailsService } from '../../../services/data-access/get-movie-details.service';
import { SearchobjectModel } from '../../models/searchobject.model';
import {  MoviedetailsModel } from '../../models/moviedetails.model';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  constructor(private getMovieDetailsService: GetMovieDetailsService) { }
  searchCompleted = false;
  limit = 200;
  responseObject: MoviedetailsModel;
  searchForm = new FormGroup({
    movieName: new FormControl('', Validators.required),
    plot: new FormControl('')
  });
  ngOnInit() {
  }

  searchMovie(form): void {
    this.setSearchObject(form);
    console.log(form);
    if (form.status === 'VALID') {
    this.getMovieDetailsService.fetchMovieDetails(this.setSearchObject(form)).subscribe((data) => {
      this.showSearchResults(data);
    });
    }
  }

  showMore() {
    if (this.limit === 200) {
      this.limit = undefined;
    } else {
      this.limit = 200;
    }
   }

  setSearchObject(form) {
    console.log(form);
    const object = new SearchobjectModel();
    object.movieName = form.value.movieName;
    object.plot = form.value.plot;
    return object;
  }

  showSearchResults(result): void {
    this.searchCompleted = true;
    this.responseObject = result;
  }
}
