import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'stringToArray'
})
export class StringToArrayPipe implements PipeTransform {

  transform(stringToConvert: string): string[] {
    return stringToConvert.split(',');
  }

}
