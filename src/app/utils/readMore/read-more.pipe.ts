import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'readMore'
})
export class ReadMorePipe implements PipeTransform {

  transform(stringToConvert: string, size: number): string {
    if (size) {
    const convertedString = stringToConvert.slice(0, size);
    return convertedString;
  } else {
    return stringToConvert;
  }
  }


}
