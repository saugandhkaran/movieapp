import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './component/template/header/header.component';
import { SearchComponent } from './component/content/search/search.component';
import { FeaturedComponent } from './component/content/featured/featured.component';
import { StringToArrayPipe } from './utils/stringToArray/string-to-array.pipe';
import { ReadMorePipe } from './utils/readMore/read-more.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SearchComponent,
    FeaturedComponent,
    StringToArrayPipe,
    ReadMorePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
