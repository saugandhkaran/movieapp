import { TestBed } from '@angular/core/testing';

import { GetMovieDetailsService } from './get-movie-details.service';

describe('GetMovieDetailsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GetMovieDetailsService = TestBed.get(GetMovieDetailsService);
    expect(service).toBeTruthy();
  });
});
