import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class GetMovieDetailsService {

  constructor(private http: HttpClient) { }

  fetchMovieDetails(searchParams) {
    let params = new HttpParams();
    params = params.append('apiKey', '6c3a2d45');
    params = params.append('t', searchParams.movieName);
    params = params.append('plot', searchParams.plot);
    return this.http.get('http://www.omdbapi.com/', {params: params}).pipe(map((response) => {
      return response;
    }));
  }
}
