# MovieApp

This app shows the details of the movie searched. It also has a featured column which shows the details of 2 movies.

## Details

The search result shows the following details:
* Title
* Year
* Rated
* Released
* Runtime
* Genre
* Director
* Writer
* Actors
* Plot (full)
* Language
* Awards
* Poster
* List of ratings
* imdbRatings
* imdbVotes
* Type

The featured page shows the following details of 2 movies:
* Title
* Year
* Awards
* Poster
* Plot(full)

## Design Idea

* I tried to show all the details in one place, so that the user doesnt need to scroll much. I tried to give most of the information in the landing page, so that they get the complete idea of the movie in a single view and scroll less.
* The Genre is shown as comma separated and below the title of the movie to give the nature of the movie while reading the title itself.
* Year and Language is also shown by the side of the movie title, so as to give a proper identification of the movie and also to know in which languages the movie is available
* The above two points has also been done keeping in mind the UX as most of the common sites tend to put the attributes below the movie name, hence helping the end user to easily find the details
* For the featured page, I have put 6 movies in an array and any two movies are randomly selected.

## Utilities

* A pipe to convert comma separated string to an array
* A pipe to limit charachters to 200 or more (Read more feature)

## How to start

* Download the application
* Run npm install
* Run ng serve
* Open localhost:4200
